import Rectangle.Rectangle;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Rectangle  rectangle1 = new Rectangle();
        Rectangle  rectangle2 = new Rectangle(2.0f,3.0f );

        System.out.println(rectangle1.toString());
        System.out.println(rectangle2.toString());

        System.out.println(rectangle1.getArea());
        System.out.println(rectangle2.getArea());

        System.out.println(rectangle1.getPerimeter());
        System.out.println(rectangle2.getPerimeter());
    }
}
